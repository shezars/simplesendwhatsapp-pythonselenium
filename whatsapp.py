from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

def element_presence(by,xpath,time):
    element_present = EC.presence_of_element_located((By.XPATH, xpath))
    WebDriverWait(browser, time).until(element_present)

def send_message(receiver,message):
    element_presence(By.XPATH,'//*[@id="side"]/div[1]/div/label/div/div[2]',30)
    receiver_element = browser.find_element(By.XPATH, '//*[@id="side"]/div[1]/div/label/div/div[2]')
    receiver_element.send_keys(receiver)
    receiver_element.send_keys("\n")
    element_presence(By.XPATH,'//*[@id="main"]/footer/div[1]/div[2]/div/div[2]',30)
    msg_box = browser.find_element(By.XPATH,'//*[@id="main"]/footer/div[1]/div[2]/div/div[2]')
    msg_box.send_keys(message)
    msg_box.send_keys('\n')
    
options = webdriver.ChromeOptions() 
options.add_argument("--user-data-dir=C:\\Users\\Ezza Rush\\AppData\\Local\\Google\\Chrome\\User Data\\")
options.add_argument('--profile-directory=Profile')
browser = webdriver.Chrome('./chromedriver_win32/chromedriver',options=options)
browser.get('https://web.whatsapp.com/')
time.sleep(15)

contact  = "6281285822280"
message  = "TEST TEKS DISINI"

send_message(contact, message)